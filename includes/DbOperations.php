<?php

class DbOperations
{
    //the database connection variable
    private $con;
    private $map = [];

    //inside constructor
    //we are getting the connection link
    function __construct()
    {
        require_once dirname(__FILE__) . '/DbConnect.php';
        $db = new DbConnect;
        $this->con = $db->connect();
        $this->setMap();
    }

    private function setMap()
    {
        $this->map['actNumber'] = '';
        $this->map['actDate'] = '';
        $this->map['clientName'] = '';
        $this->map['objectName'] = '';
        $this->map['objectAddress'] = '';
        $this->map['cadastrNumber'] = '';
        $this->map['documentStatus'] = '';
        $this->map['scanLink'] = '';
        $this->map['Lat'] = '';
        $this->map['Lon'] = '';
        $this->map['urlScan'] = '';
        $this->map['cityName'] = '';
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getLast($limit = 100)
    {
        $query = "SELECT " . implode(', ', array_keys($this->map)) . " FROM MBO_2017 ORDER BY actDate DESC LIMIT $limit;";
        $stmt = $this->con->prepare($query);
        $stmt->execute();
        $records = [];
        foreach ($stmt->get_result()->fetch_all() as $row) {
            array_push($records, array_combine(array_keys($this->map), array_values($row)));
        }
        return $records;
    }

    /**
     * @param string $fieldName
     * @param string $fieldValue
     * @param int $limit
     * @return array
     */
    public function getLike($fieldName = '', $fieldValue = '', $limit = 100)
    {
        $records = [];
        if (array_key_exists($fieldName, $this->map)) {
            $value = "%" . trim($fieldValue) . "%";
            $query = "SELECT " . implode(', ', array_keys($this->map)) . " FROM MBO_2017 WHERE $fieldName LIKE ? ORDER BY actDate DESC LIMIT $limit;";
            $stmt = $this->con->prepare($query);
            $stmt->bind_param("s", $value);
            $stmt->execute();
            foreach ($stmt->get_result()->fetch_all() as $row) {
                array_push($records, array_combine(array_keys($this->map), array_values($row)));
            }
        }
        return $records;
    }

}