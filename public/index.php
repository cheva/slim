<?php
//place this before any script you want to calculate time
$time_start = microtime(true);

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../includes/DbOperations.php';

// passing in settings in Container constructor
$container = new \Slim\Container([
    'settings' => [
        'displayErrorDetails' => true,
        'determineRouteBeforeAppMiddleware' => true,
        'debug' => true],
    'time_start' => $time_start,
]);

$app = new \Slim\App($container);

/**
 * Index page
 */
$app->get('/', function ($request, $response) {
    return file_get_contents('../templates/static/index.html');
});

/**
 * Remote maintenance script
 */
$app->get('/script', function ($request, $response) {
    shell_exec('date >> ' . __DIR__ . '/script.txt');
    shell_exec('cd ../ && git status >> ' . __DIR__ . '/script.txt');
    shell_exec('cd ../ && git add -A >> ' . __DIR__ . '/script.txt');
    shell_exec('cd ../ && git commit -m script >> ' . __DIR__ . '/script.txt');
    shell_exec('cd ../ && git pull >> ' . __DIR__ . '/script.txt');
    shell_exec('cd ../ && composer install >> ' . __DIR__ . '/script.txt');
    shell_exec('echo "---------------------------------------------------------------------" >> ' . __DIR__ . '/script.txt');
    return "<pre>" . file_get_contents(__DIR__ . '/script.txt');
});

/**
 * Get last record
 */
$app->get('/last', function (Request $request, Response $response) use ($app) {
    $db = new DbOperations;
    $records = $db->getLast(1);
    return r200($response, $records, $app);
});

/**
 * Get last 1-1000 records
 */
$app->get('/last/{limit}', function (Request $request, Response $response, array $args) use ($app) {
    $limit = (int)($args['limit']);
    $limit = (0 < $limit and $limit < 1000) ? $limit : 100;
    $db = new DbOperations;
    $records = $db->getLast($limit);
    return r200($response, $records, $app);
});

/**
 * Get by [mapped] field LIKE %value%
 */
$app->get('/mbo/{fieldName}/{fieldValue}', function (Request $request, Response $response, array $args) use ($app) {
    $db = new DbOperations;
    $records = $db->getLike($args['fieldName'], $args['fieldValue']);
    return r200($response, $records, $app);
});

/**
 * Response 200
 * @param Response $response
 * @param array $records
 * @param \Slim\App $app
 * @return Response $response
 */
function r200($response, $records, $app)
{
    $response_data = [
        'error' => false,
        'data' => $records,
        'stats' => [
            'execution_time' => sprintf('%f',microtime(true) - $app->getContainer()->get('time_start'))
        ]
    ];

    $response->write(json_encode($response_data));
    return $response
        ->withHeader('Content-type', 'application/json')
        ->withHeader('X-API-Version', '1.0')
        ->withHeader('X-Execution-Time', sprintf('%f',microtime(true) - $app->getContainer()->get('time_start')))
        ->withStatus(200);
}

$app->run();